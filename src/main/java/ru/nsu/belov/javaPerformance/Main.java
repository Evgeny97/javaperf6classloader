package ru.nsu.belov.javaPerformance;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        MyClassLoader myClassLoader = new MyClassLoader(Main.class.getClassLoader());
        File classesFolder = new File("A");
        String[] classNames = classesFolder.list();

        for (String className : classNames) {
            try {
                String moduleName = className.replaceAll(".class", "");
                Class loadedClass = myClassLoader.loadClass(moduleName);
                try {
                    Method method = loadedClass.getMethod("getSecurityMessage");
                    Object object = loadedClass.newInstance();
                    System.out.println(loadedClass.getName() + ": " + method.invoke(object));
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException ignored) {
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
