package ru.nsu.belov.javaPerformance;

import java.io.*;

public class MyClassLoader extends ClassLoader {
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        try {
            return super.findClass(name);
        } catch (ClassNotFoundException ignored) {
        }
        try {
            String path = "A\\" + name + ".class";
            byte b[] = getByteArray(path);
            return defineClass(null, b, 0, b.length);
        } catch (IOException e) {
            throw new ClassNotFoundException("IO error occurred");
        }
    }

    public MyClassLoader(ClassLoader parent) {
        super(parent);
    }

    private byte[] getByteArray(String path) throws IOException, FileNotFoundException {
        File file = new File(path);
        InputStream inputStream = new FileInputStream(file);
        int length = (int) file.length();

        byte[] bytes = new byte[length];

        int readAll = 0;
        int readLast = 0;

        while (readAll < length && (readLast = inputStream.read(bytes, readAll, length - readAll)) >= 0) {
            readAll += readLast;
        }

        if (readAll < length) {
            throw new IOException("Could not completely read file " + path);
        }

        inputStream.close();
        return bytes;
    }
}

